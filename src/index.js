import React from 'react';
import ReactDOM from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css';

/*IMPORT FROM SRC*/
import App from './App';

/*IMPORT FROM COMPONENTS*/
import ApplicationNavbar from './components/ApplicationNavbar';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

