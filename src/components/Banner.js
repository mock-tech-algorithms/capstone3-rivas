import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
/**********************************************/

export default function Banner() {
	return(

		<Row className="banner p-3 offset-none w-100">
			<Col lg={12}>
				<h1>TakeNOTE</h1>
				<p><em>You are free to go, just dont forget to leave a Note</em></p>
				<Link className="generalBtn btn" to={`/products`}>Take a Note HERE</Link>
			</Col>
		</Row>

	)
}